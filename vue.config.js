const webpack = require("webpack");
const path = require("path");

module.exports = {
  lintOnSave: false,
  transpileDependencies: ["vuetify"],
  chainWebpack: (config) => {
    config.module.rules.delete("eslint");
  },
  configureWebpack: {
    resolve: {
      alias: { "bn.js": path.join(__dirname, "node_modules/bn.js/lib/bn.js") },
    },
    plugins: [new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)],
  },
  css: { extract: { ignoreOrder: true } },
  publicPath: process.env.NODE_ENV === "production" ? "/" : "/",
};
