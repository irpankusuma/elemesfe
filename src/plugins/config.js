const f = process.env;
const env = f.NODE_ENV;
const config = {
  development: {
    VUE_APP_SID_PREFIX: "app123__",
    VUE_APP_AES_TYPE: "aes-256-gcm",
    VUE_APP_CIPHER_KEY: "Abc",
    VUE_APP_CIPHER_IV: "123",
    VUE_APP_API_KEY: "16dab97874626763838091df72196ef4",
    VUE_APP_API_KEY_V4:
      "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxNmRhYjk3ODc0NjI2NzYzODM4MDkxZGY3MjE5NmVmNCIsInN1YiI6IjYyMTMxYTZkMGJiMDc2MDA2YjcwNThjMyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.6aKB3EVL8ChGa2OYX22igC5dtJAAxA-CbOg8WwEEQ5M",
    VUE_APP_API_URL: "https://api.themoviedb.org/3/",
    VUE_APP_API_WS: "ws://",
    VUE_APP_IMAGE_URL: "http://image.tmdb.org/t/p/",
  },
  production: {
    VUE_APP_SID_PREFIX: "app123__",
    VUE_APP_AES_TYPE: "aes-256-gcm",
    VUE_APP_CIPHER_KEY: "Abc",
    VUE_APP_CIPHER_IV: "123",
    VUE_APP_API_KEY: "16dab97874626763838091df72196ef4",
    VUE_APP_API_KEY_V4:
      "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxNmRhYjk3ODc0NjI2NzYzODM4MDkxZGY3MjE5NmVmNCIsInN1YiI6IjYyMTMxYTZkMGJiMDc2MDA2YjcwNThjMyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.6aKB3EVL8ChGa2OYX22igC5dtJAAxA-CbOg8WwEEQ5M",
    VUE_APP_API_URL: "https://api.themoviedb.org/3/",
    VUE_APP_API_WS: "ws://",
    VUE_APP_IMAGE_URL: "http://image.tmdb.org/t/p/",
  },
};

module.exports = config[env];
