import Vue from "vue";
import colors from "vuetify/lib/util/colors";
import Vuetify from "vuetify/lib/framework";

Vue.use(Vuetify);
export default new Vuetify({
  theme: {
    themes: {
      options: {
        themeCache: {
          get: (key) => localStorage.getItem(key),
          set: (key, value) => localStorage.setItem(key, value),
        },
      },
      dark: {
        primary: colors.grey.darken1,
      },
      light: {
        primary: colors.grey.darken1,
      },
    },
  },
});
