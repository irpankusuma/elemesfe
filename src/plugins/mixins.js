import Vue from "vue";
import moment from "dayjs";
import axios from "axios";
import store from "@/store";
import router from "@/router";
import crypto from "crypto";
import {
  VUE_APP_AES_TYPE,
  VUE_APP_CIPHER_KEY,
  VUE_APP_CIPHER_IV,
  VUE_APP_API_KEY,
  VUE_APP_API_URL,
} from "@/plugins/config";

// axios pre-config
Vue.prototype.axios = axios;
axios.defaults.baseURL = VUE_APP_API_URL;
axios.defaults.headers.post["Content-Type"] = "application/json";
// axios.defaults.headers.post["APIKEY"] = VUE_APP_API_KEY;
// axios.defaults.headers.get["APIKEY"] = VUE_APP_API_KEY;

axios.interceptors.request.use(
  (config) => {
    store.commit("loading", true);
    return config;
  },
  (error) => {
    store.commit("loading", false);
    return Promise.reject(error);
  }
);
axios.interceptors.response.use(
  (res) => {
    store.commit("loading", false);
    if (res.status == 200) return res;
    else store.commit.snackbar({ color: "error", message: [res] });
  },
  (error) => {
    store.commit("loading", false);
    return Promise.reject(error);
  }
);

const mixin = {
  filters: {
    datetime(date) {
      return moment(date).format("DD MMM YYYY HH:mm:ss");
    },
    date(date) {
      return moment(date).format("DD MMM YYYY");
    },
    time(date) {
      return moment(date).format("HH:mm:ss");
    },
  },
  methods: {
    moment(date, format = null) {
      if (format) return moment(date).format(format);
      return moment(date);
    },
    client: async function (
      method = "GET",
      url,
      params = {},
      config = {},
      callback
    ) {
      if (method == "GET") {
        await axios
          .get(url, config)
          .then((r) => {
            if (typeof callback === "function") {
              callback(r);
            }
            if (r.data.payload && r.data.payload.is_relogin && !r.data.success)
              store.commit(`login_popup`, true);
          })
          .catch((e) => {
            store.commit("snackbar", {
              color: "error",
              message: e,
              alert: true,
            });
          });
      } else {
        await axios
          .post(url, params, config)
          .then((r) => {
            if (typeof callback === "function") {
              callback(r);
            }
            if (r.data.payload && r.data.payload.is_relogin && !r.data.success)
              store.commit(`login_popup`, true);
          })
          .catch((e) => {
            store.commit("snackbar", {
              color: "error",
              message: e,
              alert: true,
            });
          });
      }
    },
    http(method, url, params, headers = [], callback) {
      var xhr = new XMLHttpRequest();
      xhr.open(method, url, true);

      if (headers.length > 0) {
        for (let i = 0; i < headers.length; i++) {
          xhr.setRequestHeader(headers[i].key, headers[i].value);
        }
      }

      xhr.onloadstart = function () {
        store.commit("loading", true);
      };
      xhr.onload = function () {
        let arr = JSON.parse(xhr.response);
        xhr.readyState == 4 && xhr.status == 200
          ? console.log("Success.")
          : console.log("Failed.");
        if (typeof callback === "function") {
          callback(arr, xhr);
        }
      };
      xhr.onloadend = function () {
        store.commit("loading", false);
      };
      xhr.send(params);
    },
    logout() {
      Vue.vls.clear();
      store.commit("menu", false);
      router.push("/");
    },
    encryptText(text) {
      if (!text) return null;
      else {
        let cipher = crypto.createCipheriv(
          VUE_APP_AES_TYPE,
          VUE_APP_CIPHER_KEY,
          VUE_APP_CIPHER_IV
        );
        let encrypted = cipher.update(text);
        encrypted = Buffer.concat([encrypted, cipher.final()]);

        return encrypted.toString("hex");
      }
    },
    decryptText(text) {
      if (!text) return null;
      else {
        let encryptedText = Buffer.from(text, "hex");
        let decipher = crypto.createDecipheriv(
          VUE_APP_AES_TYPE,
          VUE_APP_CIPHER_KEY,
          VUE_APP_CIPHER_IV
        );
        let decrypted = decipher.update(encryptedText);

        return decrypted.toString();
      }
    },
    buildName(name) {
      const u = moment().format("DDMMYYYYHHmmss.SSS");
      const l = name.length;
      const i = name.lastIndexOf(".");
      const e = name.substring(i, l);
      name = name.substr(0, i);
      let newname = `${name}-${u}`;
      let filename = newname;
      filename = filename.replace(
        /([~!@#$%^&*()_+=.`{}\[\]\|\\:;'<>,\/? ])+/g,
        "-"
      );

      return filename + e;
    },
    getExt(val = null) {
      let xt = null,
        index = null;
      if (val) index = val.indexOf(".") !== -1 ? val.indexOf(".") : null;
      if (index) {
        xt = val.substring(index, val.length);
      }

      return xt ? xt.toLowerCase() : xt;
    },
  },
};

export default Vue.mixin(mixin);
