import Vue from "vue";
import Vx from "vuex";
import { VUE_APP_API_KEY, VUE_APP_IMAGE_URL } from "@/plugins/config";

Vue.use(Vx);
export default new Vx.Store({
  state: {
    menu: false,
    loading: false,
    alert: false,
    snackbar: false,
    message: null,
    color: "black",
    timeout: 3000,
    idle_timeout: undefined,
    login_popup: false,
    form_id: null,
    form_name: null,
    notif: [],
    filter: [],
    api_key: VUE_APP_API_KEY,
    image_url: VUE_APP_IMAGE_URL,
    item: {},
    attrs: {}
  },
  getters: {
    isMenu: (state) => state.menu,
    form_id: (state) => state.form_id,
    form_name: (state) => state.form_name,
    notif: (state) => state.notif,
    image_url: (state) => state.image_url,
    api_key: (state) => state.api_key,
    item: (state) => state.item,
    attrs: (state) => state.attrs
  },
  mutations: {
    attrs: (state, v) => (state.attrs = v),
    item: (state, v) => (state.item = v),
    notif: (state, v) => (state.notif = v),
    form_id: (state, v) => (state.form_id = v),
    form_name: (state, v) => (state.form_name = v),
    menu: (state, v) => (state.menu = v),
    loading: (state, v) => (state.loading = v),
    snackbar: (state, v) => {
      let join = ``;
      if (v.message && Array.isArray(v.message)) {
        join = `<ul><li>`;
        join = v.message.join(`</li><li>`);
        join += `</li></ul>`;
      }

      state.snackbar = true;
      state.message = v.message && Array.isArray(v.message) ? join : v.message;
      state.color = v.color;
    },
    login_popup: (state, v) => (state.login_popup = v),
  },
  actions: {},
});
