import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import vls from "vue-ls";
import router from "./router";
import store from "./store";
import "./plugins/mixins.js";
import { VUE_APP_SID_PREFIX } from "./plugins/config.js";
import "./registerServiceWorker";
import JsonExc from "vue-json-excel";

Vue.config.productionTip = false;
Vue.component("downloadExcel", JsonExc);
Vue.use(vls, {
  namespace: VUE_APP_SID_PREFIX,
  name: "vls",
  storage: "local",
});

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
