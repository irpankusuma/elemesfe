import Vue from "vue";
import Router from "vue-router";
import store from "@/store";

const guard = (to, from, next) => {
  let n = !Vue.vls.get(`sid`) ? false : true;
  store.commit("menu", n);
  if (n) next();
  else next("/");
};

Vue.use(Router);
const routes = [
  {
    path: "/",
    name: "index",
    component: () => import(`@/components/Convert.vue`),
  },
];

export default new Router({ routes });
